console.log("Hello World!");
alert("d1-s17");

// [SECTION] Functions
// Functions in javascrip are lines/blocks of codes that tell our device/ application to perform a certain task when called/invoked.

/*
	Syntax:
		function functionName() {
			code block (statement)
	}
*/

// function keyword - used to define js functions
// functionName - we set name so taht we can use it later
// function block ({}) - this is where code will be executed

function printName(/*this is called parameter*/){
	console.log("My name is John");
}

printName(); //Invocation - calling a function


// Function Expression
// A function can alse be stored in a variable. This is called a function expression.

let variableFunction = function(){
	console.log("Hello Again!");
}
variableFunction();

let funcExpression = function funcName(){
	console.log("Hello from the side.");
}
// funcName(); - invoking a functionName stored in a variable is not possible.
// always call a variable with an assigned function.

funcExpression();

function declaredFunction(){
	console.log("Hello World from declaredFunction().");
}
declaredFunction();
// re-assigning declaredfunction() value;

declaredFunction = function(){
	console.log("Updated declaredFunction");
}
declaredFunction();
// you can re-assign function expressions.

//Re-assigning function declared with constant
const constantFunc = function(){
	console.log("Initialized with const!");
}
constantFunc();

// re-assignment of a const func example
/*constantFunc = function(){
	console.log("this is gonna be an error.");
}

constantFunc();*/

// [SECTION] Function Scoping

/*
	Scope is the accessibility (visibility) of variables.

	Javascript Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
			JavaScrip has function scope: Each function creates a new scope. 
			Variables defined inside a function are not accessbile (visible)
			from outside the function.
			Variables declared with car, let and const are quite similar when declared inside a function.
*/
{
	let localVar = "Armando Perez";
}
/*console.log(localVar);  ---> This will result in an error. because (localVar is inside a code lock {}*/

let globalScope = "Mr. Worldwide";
console.log(globalScope);

//Function Scoping
function showNames(){
	//Function scoped variables:
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionConst);
	console.log(functionLet);

/*showNames();*/

}

/*showNames(); invoking a function with the same invacation like this will cause an infinite loop.*/


// Nested Functions 
// Functions within a function

function myNewFunction(){
	let name = "Jane"; //inherited data

	function nestedFunction(){
		let nestedName = "John";
		console.log(name);
	}
}


// Global Scoped Variable
let globalName = "Alendro";
function myNewFunction2(){
	let nameInside = "Renz";
	// Variables declared globally (outside any fuction/code block) have a global scope.
	// Global variables can be accessed from anywhere in a javascript
	// program including from inside a function
	console.log(globalName);

}
myNewFunction2();

// [SECTION] Alert
/*alert("Hello bro!"); //This will be executed immediately*/

/*function showAlert(){
	alert("Click OK to continue.");
}

showAlert();
*/
console.log("I will only login to the console once the alert has been dismissed.")

// Notes on the use of alert();
	// Show only an alert() for a short dialog/message to the user.
	// Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

/*let samplePrompt = prompt("Enter your Name.");
console.log("Hello " +samplePrompt+ ".")
console.log(typeof samplePrompt);*/

// prompt() can be used to gather user input
// prompt() it can be run immediately

function printWelcomeMessage(){
	let firstName = prompt("Enter your first name.");
	let lastName = prompt("Enter your last name.");

	console.log("Hello, " +firstName+ " " +lastName+ "!");
	console.log("Welcome to my page!");
}

printWelcomeMessage();

// [SECTION] Function naming convention
// Function names should be definitive of the task it will perform. It usually contains a verb.

function getCourse(){
	let courses =["Science 101", "Math 101", "English 101"];
	console.log(courses);
}

getCourse();